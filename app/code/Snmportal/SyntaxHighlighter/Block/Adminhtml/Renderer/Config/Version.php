<?php

namespace Snmportal\SyntaxHighlighter\Block\Adminhtml\Renderer\Config;

use Magento\Config\Block\System\Config\Form\Field;
use Magento\Framework\Data\Form\Element\AbstractElement;

/**
 * Class Version
 * @package Snmportal\SyntaxHighlighter\Block\Adminhtml\Renderer\Config
 */
class Version extends Field
{
    const MNAME = 'Snmportal_SyntaxHighlighter';
    /**
     * @var \Magento\Framework\App\ProductMetadataInterface
     */
    protected $productMetadata;
    /**
     * @var \Magento\Framework\Module\ModuleResource
     */
    private $moduleResource;

    /**
     * Version constructor.
     * @param \Magento\Framework\Module\ModuleResource $moduleResource
     * @param \Magento\Framework\App\ProductMetadataInterface $productMetadata
     * @param \Magento\Backend\Block\Template\Context $context
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Module\ModuleResource $moduleResource,
        \Magento\Framework\App\ProductMetadataInterface $productMetadata,
        \Magento\Backend\Block\Template\Context $context,
        array $data = []
    ) {
        $this->productMetadata = $productMetadata;
        $this->moduleResource = $moduleResource;
        parent::__construct($context, $data);
    }

    /**
     * @param AbstractElement $element
     * @return string
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    protected function _getElementHtml(AbstractElement $element)
    {
        $dVersion = $this->moduleResource->getDataVersion('Snmportal_SyntaxHighlighter');
        $info = $this->loadVersionInfo();//'Snmportal_SyntaxHighlighter');
        $html = '<div style="padding-top:7px">' . $dVersion;
        if ($info) {
            if ($info['version'] > $dVersion) {
                $html .= ' <b>' . __('New version (%1, %2) available on the server.', $info['version'], $info['date']) . '</b>';
            } else {
                $html .= ' <b>' . __('is current version.') . '</b>';
            }
        }
        $html .= '</div>';
        return $html;
    }

    /**
     * @return bool|false|mixed
     */
    private function loadVersionInfo()
    {
        $result = false;
        try {
            $cacheBackend = new \Zend_Cache_Backend();
            $cache = \Zend_Cache::factory('Core', 'File', ['lifetime' => 43200], ['cache_dir' => $cacheBackend->getTmpDir()]
            );
        } catch (\Exception $e) {
            return $result;
        }
        $cacheKey = 'snminfo_' . self::MNAME;
        $cachedHtml = $cache->load($cacheKey);

        if ($cachedHtml !== false && $cachedHtml !== '') {
            $result = $cachedHtml;
        } else {
            try {
                $modulversion = $this->moduleResource->getDataVersion('Snmportal_SyntaxHighlighter');
                $version = $this->productMetadata->getVersion();
                $url = 'https://snm-portal.com/media/module/module_version.json';
                if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
                    $jsoninfo = file_get_contents($url . '?mv=' . $version . '&ex=' . self::MNAME . '&ev=' . $modulversion);
                } else {
                    $client = new \Zend_Http_Client($url, ['timeout' => 10]);
                    $client->setParameterGet(['mv' => $version, 'ex' => self::MNAME, 'ev' => $modulversion]);
                    $jsoninfo = $client->request('GET')->getBody();
                }
                if ($jsoninfo) {
                    $jsoninfo = json_decode($jsoninfo, true);
                    if (is_array($jsoninfo) && isset($jsoninfo['module'][self::MNAME])) {
                        $result = $jsoninfo['module'][self::MNAME];
                        $cache->save($result, $cacheKey);
                    }
                }
            } catch (\Exception $e) {
            }
        }
        return $result;
    }
}
